$(document).ready(function () {
    loadData();

    $('form').submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function () {
                loadData();
                resetForm();
            }
        });
    })
})

function loadData() {
    $.get('data.php', function (data) {
        $('#content').html(data);
        $('.hapusData').click(function (e) {
            e.preventDefault();
            $.ajax({
                type: 'get',
                url: $(this).attr('href'),
                success: function () {
                    loadData();
                }
            });
        })
        $('.updateData').click(function (e) {
            e.preventDefault();
            $('[name=Name]').val($(this).attr('nama'));
            $('[name=Bidang]').val($(this).attr('bidang'));
            $('form').attr('action', $(this).attr('href'));
        })
    })
}

function resetForm() {
    $('[type=text]').val('');
    $('[name=Name]').focus();
    $('form').attr('action', 'simpan.php');
}